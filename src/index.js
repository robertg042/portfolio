import $ from "jquery";
import "sanitize.css/sanitize.css";
import "./assets/fonts/font-awesome/fontawesome.css";
import "./assets/fonts/lato/lato.css";
import "./styles/index.scss";

const animateScrollDownArrows = () => {
  const scrollDownArrows = [...document.getElementsByClassName("scroll-down")];
  let i = 0;
  const activeClass = "scroll-down-active";

  return () => {
    if (i === 3) {
      i = 0;
    }
    scrollDownArrows.forEach((element, index) => {
      if (index === i) {
        element.classList.add(activeClass);
      } else if (element.classList.contains(activeClass)) {
        element.classList.remove(activeClass);
      }
    });
    i++;
  };
};

const obCode = (x, t = false) => {
  if (t) {
    return x.replace(/[a-j]/g, c => {
      return String.fromCharCode(c.charCodeAt(0) - 49);
    });
  }

  return x.replace(/[a-zA-Z]/g, c => {
    return String.fromCharCode(
      // eslint-disable-next-line
      (c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26
    );
  });
};

const h4x0r = document.getElementById("h4x0r");
let alredyH4x0red = false;
const createUnObCode = () => {
  if (!alredyH4x0red) {
    h4x0r.innerHTML = "";

    const outer = document.createElement("div");
    const icon = document.createElement("i");
    icon.className = "fas fa-envelope";
    icon.style.marginRight = "0.7rem";
    icon.setAttribute("aria-hidden", "true");
    outer.appendChild(icon);

    const anchor = document.createElement("a");
    let h4x0rText = obCode("xbagnxg@eboregtbeavpxv.cy");
    anchor.text = `${h4x0rText}`;
    let pre = obCode("znvygb");
    anchor.setAttribute("href", `${pre}:${h4x0rText}`);
    anchor.setAttribute("rel", "nofollow");
    anchor.setAttribute("aria-labelledby", "address-label");
    outer.appendChild(anchor);

    const outer2 = document.createElement("div");
    const icon2 = document.createElement("i");
    icon2.className = "fas fa-mobile-alt";
    icon2.style.marginRight = "0.7rem";
    icon2.setAttribute("aria-hidden", "true");
    outer2.appendChild(icon2);
    const anchor2 = document.createElement("a");
    h4x0rText = obCode("+eifaegfaeaa", true);
    anchor2.text = `${h4x0rText}`;
    pre = obCode("gry");
    anchor2.setAttribute("href", `${pre}:${h4x0rText}`);
    anchor2.setAttribute("rel", "nofollow");
    anchor2.setAttribute("aria-labelledby", "address-label");
    outer2.style.paddingTop = "1rem";
    outer2.appendChild(anchor2);

    h4x0r.appendChild(outer);
    h4x0r.appendChild(outer2);

    alredyH4x0red = true;
  }
};

h4x0r.addEventListener("click", createUnObCode);
h4x0r.addEventListener("mouseover", createUnObCode);

const scrollDownArrowsContainer = document.getElementById("landing-actions");
scrollDownArrowsContainer.addEventListener("mouseover", () => {
  const scrollDownLabel = document.getElementById("scroll-down-label");
  scrollDownLabel.classList.add("shown");
});

scrollDownArrowsContainer.addEventListener("mouseout", () => {
  const scrollDownLabel = document.getElementById("scroll-down-label");
  if (scrollDownLabel.classList.contains("shown")) {
    scrollDownLabel.classList.remove("shown");
  }
});

document.addEventListener("DOMContentLoaded", () => {
  setInterval(animateScrollDownArrows(), 400);
});

$("#anchor-to-projects").on("click", () => {
  const projects = $("#projects").position().top;

  $("html, body").animate({ scrollTop: projects }, 700);
});

const anchorToProjects = document.getElementById("anchor-to-projects");
anchorToProjects.addEventListener("focus", () => {
  const scrollDownLabel = document.getElementById("scroll-down-label");
  scrollDownLabel.classList.add("shown");
});
